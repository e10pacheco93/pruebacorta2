/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebacorta;

/**
 *
 * @author root
 */
public class Motor {
    private int codigo;
    private int cilindros;
    private int hp;

    public Motor() {
    }

    public Motor(int codigo, int cilindros, int hp) {
        this.codigo = codigo;
        this.cilindros = cilindros;
        this.hp = hp;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getCilindros() {
        return cilindros;
    }

    public void setCilindros(int cilindros) {
        this.cilindros = cilindros;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    @Override
    public String toString() {
        return "Motor{" + "codigo=" + codigo + ", cilindros=" + cilindros + ", hp=" + hp + '}';
    }
    
    
    
    
}
