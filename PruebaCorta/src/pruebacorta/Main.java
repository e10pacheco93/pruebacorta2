/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebacorta;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Stack;

/**
 *
 * @author root
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ArrayList list = new ArrayList();
   
        LinkedList cola = new LinkedList();
        Stack pila = new Stack();

        Vehiculo vehi1 = new Vehiculo("Toyota", 1998, "Rojo", new Motor(101, 4, 2000));
        Vehiculo vehi2 = new Vehiculo("Toyota", 1998, "Rojo", new Motor(101, 4, 2000));
        Vehiculo vehi3 = new Vehiculo("Toyota", 1998, "Rojo", new Motor(101, 4, 2000));
        Vehiculo vehi4 = new Vehiculo("Toyota", 1998, "Rojo", new Motor(101, 4, 2000));
        Vehiculo vehi5 = new Vehiculo("Toyota", 1998, "Rojo", new Motor(101, 4, 2000));

        pila.push(vehi1);
        pila.push(vehi2);
        pila.push(vehi3);
        pila.push(vehi4);
        pila.push(vehi5);

        Persona cliente1 = new Persona(111111, "Eduardo", vehi5);
        Persona cliente2 = new Persona(222222, "Ana", vehi4);
        Persona cliente3 = new Persona(333333, "Luis", vehi3);
        Persona cliente4 = new Persona(111111, "Eduardo", vehi2);
        Persona cliente5 = new Persona(111111, "Eduardo", vehi1);
        
        
        cola.push(cliente1);
        cola.push(cliente2);
        cola.push(cliente3);
        cola.push(cliente4);
        cola.push(cliente5);
        
        for (int i = 0; i < 5; i++) {
            System.out.println("Ingresa \n");
            list .add(cola.poll() + "\n");

            System.out.println(list.get(i));
        }

    }

}
